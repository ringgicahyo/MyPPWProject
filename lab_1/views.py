from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Ringgi Cahyo Dwiputra' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,2,25) #TODO Implement this, format (Year, Month, Date)
npm = 1706025005 # TODO Implement this
tempat_kuliah = "Universitas Indonesia"
hobi = "to code a program"
desc = "I'm a funny man"
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, "tempat_kuliah" : tempat_kuliah, "hobi" : hobi, "desc" : desc}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
